const express = require('express')
const app = express()

const nodeId = Math.floor(Math.random() * 10000);
const listenPort = process.env.PORT || 3000

app.get('/pong', (req, res) => {  res.send(`pong(${nodeId})`) })

app.listen(listenPort, () => console.log(`Pong App listening on port ${listenPort}!`))
