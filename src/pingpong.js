const express = require('express')
const fetch = require("node-fetch");
const app = express()

const nodeId = Math.floor(Math.random() * 10000);
const listenPort = process.env.PORT || 3000
const pingServicePort = process.env.PING_PORT || 3000
const pongServicePort = process.env.PONG_PORT || 3000

app.get('/ping-pong', (req, res) => {
  const pingResponse = fetch(`http://ping-service:${pingServicePort}/ping`).then((response) => response.text())
  const pongResponse = fetch(`http://pong-service:${pongServicePort}/pong`).then((response) => response.text())

  Promise.all([pingResponse, pongResponse]).then((responses) => {
    res.send(`(${nodeId}) - ${responses[0]} ${responses[1]}`)
  })
});

app.listen(listenPort, () => console.log(`PingPong App listening on port ${listenPort}!`))
