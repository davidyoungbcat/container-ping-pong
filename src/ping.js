const express = require('express')
const app = express()

const nodeId = Math.floor(Math.random() * 10000);
const listenPort = process.env.PORT || 3000

app.get('/ping', (req, res) => {  res.send(`ping(${nodeId})`) })

app.listen(listenPort, () => console.log(`Ping App listening on port ${listenPort}!`))
